package englishhub.EnglishHubServer.model.dao.impl;

import englishhub.EnglishHubServer.model.dao.AbstractDAO;
import englishhub.EnglishHubServer.model.entity.AccountEntity;
import englishhub.EnglishHubServer.model.entity.UserEntity;

import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

/**
 * Клас для управління таблицею аккаунти бази даних
 */
public class AccountDAO extends AbstractDAO<AccountEntity> {

    //отримання запису
    public AccountEntity get(long id) {
        return em.find(AccountEntity.class, id);
    }

    //отримання запису по логіну
    public AccountEntity getByLogin(final String encryptedLogin) {
        TypedQuery<AccountEntity> query = em.createQuery("FROM AccountEntity E WHERE E.encryptedLogin = :encryptedLogin",
                AccountEntity.class);

        query.setParameter("encryptedLogin", encryptedLogin);

        AccountEntity result;

        try {
            result = query.getSingleResult();
        } catch (javax.persistence.NoResultException exc) {
            result = null;
        }
        return result;
    }

    //отримання всіх записів
    public List<AccountEntity> getAll() {
        TypedQuery<AccountEntity> namedQuery = em.createNamedQuery("AccountEntity.getAll", AccountEntity.class);
        return namedQuery.getResultList();
    }

    //отримання всіх записів по групі
    public List<UserEntity> getAllUsersByGroup(final long groupId) {
        TypedQuery<AccountEntity> query = em.createQuery("FROM AccountEntity E WHERE E.user.group.id = :groupId" +
                        " OR E.admin = true",
                AccountEntity.class);
        query.setParameter("groupId", groupId);

        List<AccountEntity> accounts = null;

        try {
            accounts = query.getResultList();

        } catch (javax.persistence.NoResultException exc) {
            //do nothing
        }

        List<UserEntity> users = new ArrayList<>();

        for(AccountEntity account : accounts) {
            users.add(account.getUser());
        }

        return users;
    }
}