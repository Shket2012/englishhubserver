package englishhub.EnglishHubServer.model.dao.impl;

import englishhub.EnglishHubServer.model.dao.AbstractDAO;
import englishhub.EnglishHubServer.model.entity.AccountEntity;
import englishhub.EnglishHubServer.model.entity.GroupEntity;
import englishhub.EnglishHubServer.model.entity.MessageEntity;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Клас для управління таблицею повідомлення бази даних
 */

public class MessageDAO extends AbstractDAO<MessageEntity> {

    //отримання запису
    public MessageEntity get(long id) {
        return em.find(MessageEntity.class, id);
    }

    //отримання всіх записів
    public List<MessageEntity> getAll() {
        TypedQuery<MessageEntity> namedQuery = em.createNamedQuery("MessageEntity.getAll", MessageEntity.class);
        return namedQuery.getResultList();
    }

    //отримання останнього повідомлення по групі
    public MessageEntity getLastByConversation(final long id) {
        TypedQuery<MessageEntity> query = em.createQuery("FROM MessageEntity E WHERE E.conversation.id = :conversation" +
                        " ORDER BY E.releaseDate desc",
                MessageEntity.class);
        query.setParameter("conversation", id);
        query.setMaxResults(1);

        MessageEntity result = null;

        try {
            result = query.getSingleResult();

        } catch (javax.persistence.NoResultException exc) {
            //do nothing
        }
        return result;
    }

    //отримання записів по групі
    public List<MessageEntity> getByConversation(final long idConversation) {
        TypedQuery<MessageEntity> query = em.createQuery("FROM MessageEntity E WHERE E.conversation.id = :conversation",
                MessageEntity.class);
        query.setParameter("conversation", idConversation);

        List<MessageEntity> result = null;

        try {
            result = query.getResultList();

        } catch (javax.persistence.NoResultException exc) {
            //do nothing
        }
        return result;
    }
}