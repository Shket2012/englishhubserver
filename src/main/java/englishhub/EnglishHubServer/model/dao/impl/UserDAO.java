package englishhub.EnglishHubServer.model.dao.impl;

import englishhub.EnglishHubServer.model.dao.AbstractDAO;
import englishhub.EnglishHubServer.model.entity.UserEntity;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Клас для управління таблицею груп бази даних
 */

public class UserDAO extends AbstractDAO<UserEntity> {

    //отримання запису
    public UserEntity get(long id) {
        return em.find(UserEntity.class, id);
    }

    //отримання всіх записів
    public List<UserEntity> getAll() {
        TypedQuery<UserEntity> namedQuery = em.createNamedQuery("UserEntity.getAll", UserEntity.class);
        return namedQuery.getResultList();
    }
}
