package englishhub.EnglishHubServer.model.dao.impl;

import englishhub.EnglishHubServer.model.dao.AbstractDAO;
import englishhub.EnglishHubServer.model.entity.NewsEntity;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Клас для управління таблицею новини бази даних
 */

public class NewsDAO extends AbstractDAO<NewsEntity> {

    //отримання повідомлення
    public NewsEntity get(long id) {
        return em.find(NewsEntity.class, id);
    }

    //видалення повідомлення
    public List<NewsEntity> getAll() {
        TypedQuery<NewsEntity> namedQuery = em.createNamedQuery("NewsEntity.getAll", NewsEntity.class);
        return namedQuery.getResultList();
    }
}
