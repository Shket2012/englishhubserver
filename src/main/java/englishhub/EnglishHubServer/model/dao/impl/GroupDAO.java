package englishhub.EnglishHubServer.model.dao.impl;

import englishhub.EnglishHubServer.model.dao.AbstractDAO;
import englishhub.EnglishHubServer.model.entity.GroupEntity;
import englishhub.EnglishHubServer.model.entity.UserEntity;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Клас для управління таблицею груп бази даних
 */

public class GroupDAO extends AbstractDAO<GroupEntity> {

    //отримання запису
    public GroupEntity get(long id) {
        return em.find(GroupEntity.class, id);
    }

    //отримааня всіх записів
    public List<GroupEntity> getAll() {
        TypedQuery<GroupEntity> namedQuery = em.createNamedQuery("GroupEntity.getAll", GroupEntity.class);
        return namedQuery.getResultList();
    }
}