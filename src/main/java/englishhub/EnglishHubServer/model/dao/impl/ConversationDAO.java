package englishhub.EnglishHubServer.model.dao.impl;

import englishhub.EnglishHubServer.model.dao.AbstractDAO;
import englishhub.EnglishHubServer.model.entity.ConversationEntity;
import englishhub.EnglishHubServer.model.entity.GroupEntity;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Клас для управління таблицею розмови бази даних
 */

public class ConversationDAO extends AbstractDAO<ConversationEntity> {

    //отримання записів
    public ConversationEntity get(long id) {
        return em.find(ConversationEntity.class, id);
    }

    //отримання всіх записів
    public List<ConversationEntity> getAll() {
        TypedQuery<ConversationEntity> namedQuery = em.createNamedQuery("ConversationEntity.getAll", ConversationEntity.class);
        return namedQuery.getResultList();
    }
}