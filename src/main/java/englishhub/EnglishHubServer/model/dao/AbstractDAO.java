package englishhub.EnglishHubServer.model.dao;

import englishhub.EnglishHubServer.utils.HibernateUtil;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Клас абстрактного DAO для отримання даних з бази даних
 */

public abstract class AbstractDAO<E> {
    protected EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();

    //додавання запису
    public E add(E entity) {
        em.getTransaction().begin();
        E fromDB = em.merge(entity);
        em.getTransaction().commit();
        return fromDB;
    }

    //видалення запису
    public void delete(long id) {
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    //отримання запису
    public abstract E get(long id);

    //оновлення запису
    public void update(E entity) {
        em.getTransaction().begin();
        E managedEntity = em.merge(entity);

        em.getTransaction().commit();
    }

    //отримання всіх записів
    public abstract List<E> getAll();

    //закриття підключення
    public void shutDown(){
        em.close();
    }
}
