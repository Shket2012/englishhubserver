package englishhub.EnglishHubServer.model.dao;

import englishhub.EnglishHubServer.model.dao.impl.*;

/**
 * Клас для утримання DAO
 */

public class DAOHolder {

    private AccountDAO accountDAO;
    private ConversationDAO conversationDAO;
    private GroupDAO groupDAO;
    private MessageDAO messageDAO;
    private NewsDAO newsDAO;
    private UserDAO userDAO;

    public DAOHolder() {
        accountDAO = new AccountDAO();
        conversationDAO = new ConversationDAO();
        groupDAO = new GroupDAO();
        messageDAO = new MessageDAO();
        newsDAO = new NewsDAO();
        userDAO = new UserDAO();
    }

    //Закриває підключення кожного DAO
    public void shutDown() {
        accountDAO.shutDown();
        conversationDAO.shutDown();
        groupDAO.shutDown();
        messageDAO.shutDown();
        newsDAO.shutDown();
        userDAO.shutDown();
    }

    public AccountDAO getAccountDAO() {
        return accountDAO;
    }

    public ConversationDAO getConversationDAO() {
        return conversationDAO;
    }

    public GroupDAO getGroupDAO() {
        return groupDAO;
    }

    public MessageDAO getMessageDAO() {
        return messageDAO;
    }

    public NewsDAO getNewsDAO() {
        return newsDAO;
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }
}
