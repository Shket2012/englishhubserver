package englishhub.EnglishHubServer.model.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Клас сутності розмова
 */

@Entity
@Table(name = "conversation")
@NamedQuery(name = "ConversationEntity.getAll", query = "SELECT c from ConversationEntity c")
public class ConversationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToMany
    @JoinColumn(name = "id_user")
    private List<UserEntity> users = new ArrayList<UserEntity>();

    @Column(name = "title")
    private String title;

    public ConversationEntity() {}
    public ConversationEntity(final String title, final List<UserEntity> users) {
        this.title = title;
        this.users = users;
    }

    public long getId() {
        return id;
    }

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
