package englishhub.EnglishHubServer.model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Клас сутності користувач
 */

@Entity
@Table(name = "user")
@NamedQuery(name = "UserEntity.getAll", query = "SELECT c from UserEntity c")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "fist_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "birth_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;

    @Column(name = "image_name")
    private String imageName;

    @ManyToOne
    @JoinColumn(name = "id_group")
    private GroupEntity group;

    public UserEntity() {}

    public UserEntity(final String lastName, final String middleName, final String firstName,
                      final Date birthDate) {
        this.lastName = lastName;
        this.middleName = middleName;
        this.firstName = firstName;
        this.birthDate = birthDate;
    }

    public UserEntity(final String lastName, final String middleName, final String firstName,
                      final Date birthDate, final GroupEntity group) {
        this(lastName, middleName, firstName, birthDate);
        this.group = group;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public GroupEntity getGroup() {
        return group;
    }

    public void setGroup(GroupEntity group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "LastName: " + lastName + "\n" +
                "MiddleName: " + middleName + "\n" +
                "FirstName: " + firstName + "\n" +
                "BirthDate: " + birthDate + "\n";
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}