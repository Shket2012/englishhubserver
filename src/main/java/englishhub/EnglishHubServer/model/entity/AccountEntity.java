package englishhub.EnglishHubServer.model.entity;

import javax.persistence.*;

/**
 * Клас сутності аккаунт
 */

@Entity
@Table(name = "account")
@NamedQuery(name = "AccountEntity.getAll", query = "SELECT c from AccountEntity c")
public class AccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    @JoinColumn(name = "id_user", unique = true)
    private UserEntity user;

    @Column(name = "encrypted_login", unique = true)
    private String encryptedLogin;

    @Column(name = "encrypted_password")
    private String encryptedPassword;

    @Column(name = "is_admin")
    private boolean admin;

    public AccountEntity() {}
    public AccountEntity(UserEntity user, String encryptedLogin, String encryptedPassword) {
        this.user = user;
        this.encryptedLogin = encryptedLogin;
        this.encryptedPassword = encryptedPassword;
    }

    public AccountEntity(UserEntity user, String encryptedLogin, String encryptedPassword, boolean isAdmin) {
        this(user, encryptedLogin, encryptedPassword);
        this.admin = isAdmin;
    }

    public long getId() {
        return id;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getEncryptedLogin() {
        return encryptedLogin;
    }

    public void setEncryptedLogin(String encryptedLogin) {
        this.encryptedLogin = encryptedLogin;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    @Override
    public String toString() {
        return "Id: " + id + "\n" +
                "User: " + user.toString() + "\n" +
                "Encrypted Login Password: " + encryptedPassword + "\n";
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
