package englishhub.EnglishHubServer.model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Клас сутності група
 */

@Entity
@Table(name = "user_group")
@NamedQuery(name = "GroupEntity.getAll", query = "SELECT c from GroupEntity c")
public class GroupEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "level")
    private int level;

    @Column(name = "schedule", length = 500)
    private String schedule;

    @Column(name = "training_materials", length = 500)
    private String trainingMaterials;

    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;

    public GroupEntity() {}
    public GroupEntity(final String name, final int level, final String schedule, final String trainingMaterials,
                       final Date startDate, final Date endDate) {
        this.name = name;
        this.level = level;
        this.schedule = schedule;
        this.trainingMaterials = trainingMaterials;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTrainingMaterials() {
        return trainingMaterials;
    }

    public void setTrainingMaterials(String trainingMaterials) {
        this.trainingMaterials = trainingMaterials;
    }
}