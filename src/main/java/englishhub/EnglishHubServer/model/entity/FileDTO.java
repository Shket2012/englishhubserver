package englishhub.EnglishHubServer.model.entity;

/**
 * Клас файлу для передачі
 */

public class FileDTO {

    private String name;
    private byte[] file;

    public FileDTO() {}
    public FileDTO(String name, byte[] file) {
        this.name = name;
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }
}
