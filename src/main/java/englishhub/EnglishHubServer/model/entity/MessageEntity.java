package englishhub.EnglishHubServer.model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Клас сутності повідомлення
 */

@Entity
@Table(name = "message")
@NamedQuery(name = "MessageEntity.getAll", query = "SELECT c from MessageEntity c")
public class MessageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    @JoinColumn(name = "id_conversation")
    private ConversationEntity conversation;

    @ManyToOne
    @JoinColumn(name = "id_author")
    private UserEntity author;

    @Column(name = "message")
    private String message;

    @Column(name = "release_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date releaseDate;

    public long getId() {
        return id;
    }

    public ConversationEntity getConversation() {
        return conversation;
    }

    public void setConversation(ConversationEntity conversation) {
        this.conversation = conversation;
    }

    public UserEntity getAuthor() {
        return author;
    }

    public void setAuthor(UserEntity author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}