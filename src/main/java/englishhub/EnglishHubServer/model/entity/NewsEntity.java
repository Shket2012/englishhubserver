package englishhub.EnglishHubServer.model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Клас сутності новина
 */

@Entity
@Table(name = "news")
@NamedQuery(name = "NewsEntity.getAll", query = "SELECT c from NewsEntity c order by c.releaseDate desc")
public class NewsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "content", length = 1000)
    private String content;

    @Column(name = "image_name")
    private String imageName;

    @Column(name = "release_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date releaseDate;

    public NewsEntity() { }

    public NewsEntity(String title, String content, Date releaseDate) {
        this.title = title;
        this.content = content;
        this.releaseDate = releaseDate;
    }

    public NewsEntity(String title, String content, Date releaseDate, String imageName) {
        this.title = title;
        this.content = content;
        this.releaseDate = releaseDate;
        this.imageName = imageName;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @Override
    public String toString() {
        return "Title: " + title + "\n" +
                "Content: " + content + "\n" +
                "ReleaseDate: " + releaseDate + "\n";
    }
}