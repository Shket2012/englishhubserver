package englishhub.EnglishHubServer.controller;

import englishhub.EnglishHubServer.model.dao.impl.AccountDAO;
import englishhub.EnglishHubServer.model.dao.impl.GroupDAO;
import englishhub.EnglishHubServer.model.dao.impl.UserDAO;
import englishhub.EnglishHubServer.model.entity.AccountEntity;
import englishhub.EnglishHubServer.model.entity.FileDTO;
import englishhub.EnglishHubServer.model.entity.UserEntity;
import englishhub.EnglishHubServer.utils.Authorization;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Клас-контролер для обробки запитів пов'язаних з файлами
 */


@RestController
public class FileController extends AbstractController {

    /**
     * Метод обробки запиту на завантаження файлу
     */

    @RequestMapping(value = "/files/upload", method = RequestMethod.POST)
    public ResponseEntity<Boolean> create(HttpEntity<FileDTO> entity) {
        HttpHeaders headers = entity.getHeaders();

        if (authorization.checkAccess(headers, false)) {

            FileDTO fileDTO = entity.getBody();

            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = new FileOutputStream(fileDTO.getName());
                fileOutputStream.write(fileDTO.getFile());
                fileOutputStream.close();

                Logger logger = Logger.getLogger("FileController Logger");
                logger.log(Level.INFO, "File is uploaded: " + fileDTO.getName());

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return new ResponseEntity<Boolean>(false, HttpStatus.OK);
            } catch (IOException exc) {
                exc.printStackTrace();
                return new ResponseEntity<Boolean>(false, HttpStatus.OK);
            }

            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        }

        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання файлу
     */

    @RequestMapping(value = "/files/download", method = RequestMethod.POST)
    public ResponseEntity<FileDTO> get(HttpEntity<String> entity) {
        HttpHeaders headers = entity.getHeaders();

        if (authorization.checkAccess(headers, false)) {

            byte[] data;

            String namePath = entity.getBody();
            Path fileLocation = Paths.get(namePath);
            try {
                data = Files.readAllBytes(fileLocation);
            } catch (NoSuchFileException e) {
              Logger logger = Logger.getLogger("FileController Logger");
              logger.log(Level.WARNING, "No such file: " + namePath);
              return new ResponseEntity<>(HttpStatus.OK);
            } catch (IOException e) {
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.OK);
            }

            return new ResponseEntity<>(new FileDTO(namePath, data), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
