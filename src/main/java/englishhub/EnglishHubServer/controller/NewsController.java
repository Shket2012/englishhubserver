package englishhub.EnglishHubServer.controller;

import englishhub.EnglishHubServer.model.dao.impl.NewsDAO;
import englishhub.EnglishHubServer.model.entity.NewsEntity;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Клас-контролер для обробки запитів пов'язаних з записами новин
 */

@RestController
public class NewsController extends AbstractController {

    /**
     * Метод обробки запиту на отримання всіх новин
     */

    @GetMapping("/news")
    public ResponseEntity<List<NewsEntity>> getAll(HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, false)){
            NewsDAO newsDAO = daoHolder.getNewsDAO();
            return new ResponseEntity<List<NewsEntity>>(newsDAO.getAll(), HttpStatus.OK);
        }
        return new ResponseEntity<List<NewsEntity>>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання всіх аккаунтів
     */

    @RequestMapping(value = "/news/{id}", method = RequestMethod.GET)
    public ResponseEntity<NewsEntity> get(@PathVariable("id") long id, HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, false)) {
            NewsDAO newsDAO = daoHolder.getNewsDAO();
            return new ResponseEntity<NewsEntity>(newsDAO.get(id), HttpStatus.OK);
        }
        return new ResponseEntity<NewsEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на створення новини
     */

    @RequestMapping(value = "/news", method = RequestMethod.POST)
    public ResponseEntity<NewsEntity> create(HttpEntity<NewsEntity> entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)){
            NewsDAO newsDAO = daoHolder.getNewsDAO();
            NewsEntity newsEntity = newsDAO.add(entity.getBody());
            return new ResponseEntity<NewsEntity>(newsEntity, HttpStatus.OK);
        }

        return new ResponseEntity<NewsEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на видалення новини
     */

    @RequestMapping(value = "/news/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable("id") long id, HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            NewsDAO newsDAO = daoHolder.getNewsDAO();
            Boolean result;

            if (newsDAO.get(id) == null) {
                result = false;
            }
            result = true;
            newsDAO.delete(id);

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання оновлення новин
     */

    @RequestMapping(value = "/news/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> update(@PathVariable long id, HttpEntity<NewsEntity> entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            NewsDAO newsDAO = daoHolder.getNewsDAO();
            Boolean result;

            if (newsDAO.get(id) == null) {
                result = false;
            }
            result = true;
            newsDAO.update(entity.getBody());

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }
}
