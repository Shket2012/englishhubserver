package englishhub.EnglishHubServer.controller;

import englishhub.EnglishHubServer.model.dao.DAOHolder;
import englishhub.EnglishHubServer.utils.Authorization;

import java.util.logging.Logger;

/**
 * Абстрактний контролер
 */

public abstract class AbstractController {

    //авторизація
    protected static Authorization authorization;

    //data access object's holder
    protected static DAOHolder daoHolder = new DAOHolder();

    public AbstractController(){
        authorization = new Authorization(daoHolder.getAccountDAO());
    }
}
