package englishhub.EnglishHubServer.controller;

import antlr.debug.MessageEvent;
import englishhub.EnglishHubServer.model.dao.impl.AccountDAO;
import englishhub.EnglishHubServer.model.dao.impl.ConversationDAO;
import englishhub.EnglishHubServer.model.dao.impl.MessageDAO;
import englishhub.EnglishHubServer.model.entity.AccountEntity;
import englishhub.EnglishHubServer.model.entity.ConversationEntity;
import englishhub.EnglishHubServer.model.entity.MessageEntity;
import englishhub.EnglishHubServer.model.entity.UserEntity;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Клас-контролер для обробки запитів пов'язаних з записами розмов
 */

@RestController
public class ConversationController extends AbstractController {

    private static final String LOGGER_NAME = "ConversationController Logger";

    /**
     * Метод обробки запиту на отримання всіх розмов
     */

    @GetMapping("/conversations")
    public ResponseEntity<List<ConversationEntity>> getAll(HttpEntity httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        if (authorization.checkAccess(headers, true)) {
            ConversationDAO conversationDAO = daoHolder.getConversationDAO();
            List<ConversationEntity> conversationEntities = conversationDAO.getAll();

            return new ResponseEntity<List<ConversationEntity>>(conversationEntities, HttpStatus.OK);
        }
        return new ResponseEntity<List<ConversationEntity>>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/conversations/leave/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> leave(@PathVariable("id") long id, HttpEntity httpEntity) {
        Logger logger = Logger.getLogger(LOGGER_NAME);

        HttpHeaders headers = httpEntity.getHeaders();

        String login = headers.get("login").get(0);
        String password = headers.get("password").get(0);

        if (authorization.checkAccess(login, password, false)) {
            ConversationDAO conversationDAO = daoHolder.getConversationDAO();
            ConversationEntity receivedConversation = conversationDAO.get(id);

            AccountDAO accountDAO = daoHolder.getAccountDAO();
            UserEntity user = accountDAO.getByLogin(login).getUser();


            if (receivedConversation != null) {
                List<UserEntity> users = receivedConversation.getUsers();

                for (int i = 0; i < users.size(); i++) {
                    if (users.get(i).getId() == user.getId()) {
                        users.remove(i);

                        logger.log(Level.INFO, "User: " + user.getId() + " left conversation: " + id);
                        break;
                    }
                }

                if (users.size() == 0) {
                    deleteConversation(id);
                    logger.log(Level.INFO, "Conversation was deleted: " + id);
                }

            } else {
                logger.log(Level.INFO, "Conversation didn't found: " + id);
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    private void deleteConversation(long id) {
        deleteMessagesByConversation(id);

        ConversationDAO conversationDAO = daoHolder.getConversationDAO();
        conversationDAO.delete(id);
    }

    private void deleteMessagesByConversation(long id) {
        MessageDAO messageDAO = daoHolder.getMessageDAO();
        List<MessageEntity> messages = messageDAO.getByConversation(id);

        for(MessageEntity m : messages) {
            messageDAO.delete(m.getId());
        }
    }

    /**
     * Метод обробки запиту на отримання всіх персональних розмов
     */

    @GetMapping("/conversations/personal")
    public ResponseEntity<List<ConversationEntity>> getAllPersonal(HttpEntity httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        String login = headers.get("login").get(0);
        String password = headers.get("password").get(0);

        if (authorization.checkAccess(login, password, false)) {
            ConversationDAO conversationDAO = daoHolder.getConversationDAO();

            AccountDAO accountDAO = daoHolder.getAccountDAO();
            AccountEntity accountEntity = accountDAO.getByLogin(login);

            List<ConversationEntity> conversations = conversationDAO.getAll();

            return new ResponseEntity<List<ConversationEntity>>(findConversationsByUserId(accountEntity.getUser().getId(),
                    conversations), HttpStatus.OK);
        }
        return new ResponseEntity<List<ConversationEntity>>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод пошуку розмов за користувачем
     */

    private List<ConversationEntity> findConversationsByUserId(long userId, List<ConversationEntity> conversations) {
        Stream<ConversationEntity> stream = conversations.stream();

        return stream.filter(new Predicate<ConversationEntity>() {
            @Override
            public boolean test(ConversationEntity conversationEntity) {
                List<UserEntity> users = conversationEntity.getUsers();

                for (UserEntity user : users) {
                    if (userId == user.getId()) {
                        return true;
                    }
                }
                return false;
            }
        }).collect(Collectors.toList());
    }

    /**
     * Метод обробки запиту на отримання розмови
     */

    @RequestMapping(value = "/conversations/{id}", method = RequestMethod.GET)
    public ResponseEntity<ConversationEntity> get(@PathVariable("id") long id, HttpEntity httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        if (authorization.checkAccess(headers, true)) {
            ConversationDAO conversationDAO = daoHolder.getConversationDAO();
            ConversationEntity conversationEntity = conversationDAO.get(id);

            return new ResponseEntity<ConversationEntity>(conversationEntity, HttpStatus.OK);
        }
        return new ResponseEntity<ConversationEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на створення розмови
     */

    @RequestMapping(value = "/conversations", method = RequestMethod.POST)
    public ResponseEntity<Long> create(HttpEntity<ConversationEntity> entity) {
        HttpHeaders headers = entity.getHeaders();

        if (authorization.checkAccess(headers, false)) {
            ConversationDAO conversationDAO = daoHolder.getConversationDAO();
            ConversationEntity addedEntity = conversationDAO.add(entity.getBody());

            return new ResponseEntity<Long>(addedEntity.getId(), HttpStatus.OK);
        }

        return new ResponseEntity<Long>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на видалення розмови
     */

    @RequestMapping(value = "/conversations/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable("id") long id, HttpEntity httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        if (authorization.checkAccess(headers, true)) {
            ConversationDAO conversationDAO = daoHolder.getConversationDAO();

            Boolean result = isEntityNull(id, conversationDAO);

            if (!result) {
                conversationDAO.delete(id);
            }

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на оновлення запису
     */

    @RequestMapping(value = "/conversations/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> update(@PathVariable long id, HttpEntity<ConversationEntity> httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        if (authorization.checkAccess(headers, true)) {
            ConversationDAO conversationDAO = daoHolder.getConversationDAO();

            Boolean result = isEntityNull(id, conversationDAO);

            if (!result) {
                conversationDAO.update(httpEntity.getBody());
            }

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }

    //перевірка на нуль

    private boolean isEntityNull(long id, ConversationDAO conversationDAO) {
        return (conversationDAO.get(id) == null);
    }
}
