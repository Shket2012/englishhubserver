package englishhub.EnglishHubServer.controller;

import englishhub.EnglishHubServer.model.dao.impl.AccountDAO;
import englishhub.EnglishHubServer.model.dao.impl.GroupDAO;
import englishhub.EnglishHubServer.model.dao.impl.UserDAO;
import englishhub.EnglishHubServer.model.entity.AccountEntity;
import englishhub.EnglishHubServer.model.entity.GroupEntity;
import englishhub.EnglishHubServer.model.entity.UserEntity;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Клас-контролер для обробки запитів пов'язаних з записами аккаунтів
 */

@RestController
public class AccountController extends AbstractController {

    /**
     * Метод обробки запиту на отримання всіх аккаунтів
     */

    @GetMapping("/accounts")
    public ResponseEntity<List<AccountEntity>> getAll(HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)){
            AccountDAO accountDAO = daoHolder.getAccountDAO();
            List<AccountEntity> accountEntities = accountDAO.getAll();

            return new ResponseEntity<List<AccountEntity>>(accountEntities, HttpStatus.OK);
        }
        return new ResponseEntity<List<AccountEntity>>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання аккаунту по логіну
     */

    @RequestMapping(value = "/accounts/current/", method = RequestMethod.GET)
    public ResponseEntity<AccountEntity> getByHttpHeaderLogin(HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, false)) {
            AccountDAO accountDAO = daoHolder.getAccountDAO();
            AccountEntity accountEntity = accountDAO.getByLogin(headers.get("login").get(0));

            return new ResponseEntity<>(accountEntity, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на створення аккаунту
     */

    @RequestMapping(value = "/accounts", method = RequestMethod.POST)
    public ResponseEntity<Long> create(HttpEntity<AccountEntity> entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)){
            AccountDAO accountDAO = daoHolder.getAccountDAO();
            UserDAO userDAO = daoHolder.getUserDAO();

            AccountEntity accountEntity = entity.getBody();
            UserEntity userEntity = accountEntity.getUser();

            UserEntity addedUser = userDAO.add(userEntity);
            accountEntity.setUser(addedUser);

            AccountEntity returnedAccountEntity = accountDAO.add(accountEntity);

            return new ResponseEntity<Long>(returnedAccountEntity.getId(), HttpStatus.OK);
        }

        return new ResponseEntity<Long>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання аккаунту
     */

    @RequestMapping(value = "/accounts/{id}", method = RequestMethod.GET)
    public ResponseEntity<AccountEntity> get(@PathVariable("id") long id, HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            AccountDAO accountDAO = daoHolder.getAccountDAO();
            AccountEntity accountEntity = accountDAO.get(id);

            return new ResponseEntity<AccountEntity>(accountEntity, HttpStatus.OK);
        }
        return new ResponseEntity<AccountEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на авторизацію
     */

    @GetMapping("/accounts/authorize")
    public ResponseEntity<AccountEntity> authorize(HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        String login = headers.get("login").get(0);
        String password = headers.get("password").get(0);

        if(authorization.checkAccess(login, password, false)){
            AccountDAO accountDAO = daoHolder.getAccountDAO();
            AccountEntity accountEntity = accountDAO.getByLogin(login);

            return new ResponseEntity<AccountEntity>(accountEntity, HttpStatus.OK);
        }
        return new ResponseEntity<AccountEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на видалення
     */

    @RequestMapping(value = "/accounts/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable("id") long id, HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            AccountDAO accountDAO = daoHolder.getAccountDAO();

            Boolean result;

            AccountEntity accountEntity = accountDAO.get(id);

            if (accountEntity == null) {
                result = false;
            }
            result = true;

            accountDAO.delete(id);

            UserDAO userDAO = daoHolder.getUserDAO();
            userDAO.delete(accountEntity.getUser().getId());

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на оновлення аккаунту
     */

    @RequestMapping(value = "/accounts/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> update(@PathVariable long id, HttpEntity<AccountEntity> entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            AccountDAO accountDAO = daoHolder.getAccountDAO();
            UserDAO userDAO = daoHolder.getUserDAO();
            GroupDAO groupDAO = daoHolder.getGroupDAO();

            AccountEntity accountFromBody = entity.getBody();
            AccountEntity receivedAccount = accountDAO.get(accountFromBody.getId());

            if (receivedAccount != null) {
                updateAccount(receivedAccount, accountFromBody);

                return new ResponseEntity<>(true, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    private void updateAccount(AccountEntity receivedAccount, AccountEntity accountFromBody) {
        AccountDAO accountDAO = daoHolder.getAccountDAO();
        UserDAO userDAO = daoHolder.getUserDAO();
        GroupDAO groupDAO = daoHolder.getGroupDAO();

        receivedAccount.setEncryptedLogin(accountFromBody.getEncryptedLogin());
        receivedAccount.setEncryptedPassword(accountFromBody.getEncryptedPassword());
        receivedAccount.setAdmin(accountFromBody.isAdmin());

        UserEntity userFromBody = accountFromBody.getUser();
        UserEntity receivedUser = userDAO.get(userFromBody.getId());

        receivedUser.setLastName(userFromBody.getLastName());
        receivedUser.setFirstName(userFromBody.getFirstName());
        receivedUser.setMiddleName(userFromBody.getMiddleName());
        receivedUser.setBirthDate(userFromBody.getBirthDate());
        receivedUser.setImageName(userFromBody.getImageName());

        GroupEntity groupFromBody = userFromBody.getGroup();
        GroupEntity receivedGroup = groupDAO.get(groupFromBody.getId());

        receivedGroup.setName(groupFromBody.getName());
        receivedGroup.setLevel(groupFromBody.getLevel());
        receivedGroup.setEndDate(groupFromBody.getEndDate());
        receivedGroup.setStartDate(groupFromBody.getStartDate());
        receivedGroup.setSchedule(groupFromBody.getSchedule());
        receivedGroup.setTrainingMaterials(groupFromBody.getTrainingMaterials());

        receivedUser.setGroup(receivedGroup);
        receivedAccount.setUser(receivedUser);

        userDAO.update(receivedUser);
        groupDAO.update(receivedGroup);
        accountDAO.update(receivedAccount);
    }

    /**
     * Метод обробки запиту на оновлення поточного аккаунту
     */

    @RequestMapping(value = "/accounts/current", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> updateCurrent(HttpEntity<AccountEntity> entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, false)) {
            AccountDAO accountDAO = daoHolder.getAccountDAO();
            UserDAO userDAO = daoHolder.getUserDAO();
            GroupDAO groupDAO = daoHolder.getGroupDAO();

            AccountEntity accountFromBody = entity.getBody();
            AccountEntity receivedAccount = accountDAO.get(accountFromBody.getId());

            if (receivedAccount != null) {
                updateAccount(receivedAccount, accountFromBody);

                return new ResponseEntity<>(true, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
