package englishhub.EnglishHubServer.controller;

import englishhub.EnglishHubServer.model.dao.impl.GroupDAO;
import englishhub.EnglishHubServer.model.entity.GroupEntity;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Клас-контролер для обробки запитів пов'язаних з групами
 */

@RestController
public class GroupController extends AbstractController {

    /**
     * Метод обробки запиту на отримання всіх груп
     */

    @GetMapping("/groups")
    public ResponseEntity<List<GroupEntity>> getAll(HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)){
            GroupDAO groupDAO = daoHolder.getGroupDAO();
            return new ResponseEntity<List<GroupEntity>>(groupDAO.getAll(), HttpStatus.OK);
        }
        return new ResponseEntity<List<GroupEntity>>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання групи
     */

    @RequestMapping(value = "/groups/{id}", method = RequestMethod.GET)
    public ResponseEntity<GroupEntity> get(@PathVariable("id") long id, HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            GroupDAO groupDAO = daoHolder.getGroupDAO();
            return new ResponseEntity<GroupEntity>(groupDAO.get(id), HttpStatus.OK);
        }
        return new ResponseEntity<GroupEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на створення групи
     */

    @RequestMapping(value = "/groups", method = RequestMethod.POST)
    public ResponseEntity<GroupEntity> create(HttpEntity<GroupEntity> entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)){
            GroupDAO groupDAO = daoHolder.getGroupDAO();
            GroupEntity groupEntity = groupDAO.add(entity.getBody());
            return new ResponseEntity<GroupEntity>(groupEntity, HttpStatus.OK);
        }

        return new ResponseEntity<GroupEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на видалення групи
     */

    @RequestMapping(value = "/groups/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable("id") long id, HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            GroupDAO groupDAO = daoHolder.getGroupDAO();

            Boolean result;

            if (groupDAO.get(id) == null) {
                result = false;
            }
            result = true;
            groupDAO.delete(id);

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на оновлення групи
     */

    @RequestMapping(value = "/groups/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> update(@PathVariable long id, HttpEntity<GroupEntity> entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            GroupDAO groupDAO = daoHolder.getGroupDAO();

            Boolean result;

            if (groupDAO.get(id) == null) {
                result = false;
            }
            result = true;
            groupDAO.update(entity.getBody());

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }
}
