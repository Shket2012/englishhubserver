package englishhub.EnglishHubServer.controller;

import englishhub.EnglishHubServer.model.dao.impl.MessageDAO;
import englishhub.EnglishHubServer.model.entity.MessageEntity;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Клас-контролер для обробки запитів пов'язаних з записами розмов
 */

@RestController
public class MessageController extends AbstractController {

    /**
     * Метод обробки запиту на отримання всіх повідомлень
     */

    @GetMapping("/messages")
    public ResponseEntity<List<MessageEntity>> getAll(HttpEntity httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        if(authorization.checkAccess(headers, true)){
            MessageDAO messageDAO = daoHolder.getMessageDAO();
            List<MessageEntity> messages = messageDAO.getAll();
            
            return new ResponseEntity<List<MessageEntity>>(messages, HttpStatus.OK);
        }
        return new ResponseEntity<List<MessageEntity>>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання всіх повідомлень по розмові
     */

    @RequestMapping(value = "/messages/byconversation/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<MessageEntity>> getByConversation(@PathVariable("id") long id, HttpEntity httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        if(authorization.checkAccess(headers, false)){
            MessageDAO messageDAO = daoHolder.getMessageDAO();
            return new ResponseEntity<List<MessageEntity>>(messageDAO.getByConversation(id), HttpStatus.OK);
        }
        return new ResponseEntity<List<MessageEntity>>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання всіх персональних повідомлень
     */

    @RequestMapping(value = "/messages/last", method = RequestMethod.POST)
    public ResponseEntity<List<MessageEntity>> getPersonalLastMessages(HttpEntity<List<Long>> httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        if(authorization.checkAccess(headers, false)){
            List<Long> conversationIds = httpEntity.getBody();

            List<MessageEntity> lastMessages = new ArrayList<>();

            for (Long id : conversationIds) {
                MessageDAO messageDAO = daoHolder.getMessageDAO();
                lastMessages.add(messageDAO.getLastByConversation(id));
            }

            return new ResponseEntity<List<MessageEntity>>(lastMessages, HttpStatus.OK);
        }
        return new ResponseEntity<List<MessageEntity>>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання повідомлення
     */

    @RequestMapping(value = "/messages/{id}", method = RequestMethod.GET)
    public ResponseEntity<MessageEntity> get(@PathVariable("id") long id, HttpEntity httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            MessageDAO messageDAO = daoHolder.getMessageDAO();
            return new ResponseEntity<MessageEntity>(messageDAO.get(id), HttpStatus.OK);
        }
        return new ResponseEntity<MessageEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на створення повідомлення
     */

    @RequestMapping(value = "/messages", method = RequestMethod.POST)
    public ResponseEntity<Long> create(HttpEntity<MessageEntity> entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, false)){
            MessageDAO messageDAO = daoHolder.getMessageDAO();
            MessageEntity addedEntity = messageDAO.add(entity.getBody());
            return new ResponseEntity<Long>(addedEntity.getId(), HttpStatus.OK);
        }

        return new ResponseEntity<Long>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на видалення повідомлення
     */

    @RequestMapping(value = "/messages/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable("id") long id, HttpEntity httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            MessageDAO messageDAO = daoHolder.getMessageDAO();

            Boolean result = isEntityNull(id, messageDAO);

            if(!result) {
                messageDAO.delete(id);
            }

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на оновлення повідомлення
     */

    @RequestMapping(value = "/messages/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> update(@PathVariable long id, HttpEntity<MessageEntity> httpEntity) {
        HttpHeaders headers = httpEntity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            MessageDAO messageDAO = daoHolder.getMessageDAO();

            Boolean result = isEntityNull(id, messageDAO);

            if(!result) {
                messageDAO.update(httpEntity.getBody());
            }

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }

    private boolean isEntityNull(long id, MessageDAO messageDAO) {
        return (messageDAO.get(id) == null);
    }
}
