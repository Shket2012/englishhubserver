package englishhub.EnglishHubServer.controller;

import englishhub.EnglishHubServer.model.dao.impl.AccountDAO;
import englishhub.EnglishHubServer.model.dao.impl.UserDAO;
import englishhub.EnglishHubServer.model.entity.UserEntity;
import englishhub.EnglishHubServer.utils.Authorization;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Клас-контролер для обробки запитів пов'язаних з записами користувачами
 */

@RestController
public class UserController extends AbstractController {

    /**
     * Метод обробки запиту на отримання всіх користувачів
     */

    @GetMapping("/users")
    public ResponseEntity<List<UserEntity>> getAll(HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)){
            UserDAO userDAO = daoHolder.getUserDAO();
            return new ResponseEntity<List<UserEntity>>(userDAO.getAll(), HttpStatus.OK);
        }
        return new ResponseEntity<List<UserEntity>>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання всіх користувачів по групі
     */

    @RequestMapping(value = "/users/bygroup/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<UserEntity>> getAllUsersByGroup(@PathVariable("id") long idGroup, HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, false)){
            AccountDAO accountDAO = daoHolder.getAccountDAO();
            return new ResponseEntity<List<UserEntity>>(accountDAO.getAllUsersByGroup(idGroup), HttpStatus.OK);
        }
        return new ResponseEntity<List<UserEntity>>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання користувача
     */

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserEntity> getUser(@PathVariable("id") long id, HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            UserDAO userDAO = daoHolder.getUserDAO();
            return new ResponseEntity<UserEntity>(userDAO.get(id), HttpStatus.OK);
        }
        return new ResponseEntity<UserEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на отримання користувача по логіну
     */

    @RequestMapping(value = "/users/current/", method = RequestMethod.GET)
    public ResponseEntity<UserEntity> getByHttpHeaderLogin(HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, false)) {
            AccountDAO accountDAO = daoHolder.getAccountDAO();
            UserEntity userEntity = accountDAO.getByLogin(headers.get("login").get(0)).getUser();

            return new ResponseEntity<UserEntity>(userEntity, HttpStatus.OK);
        }
        return new ResponseEntity<UserEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на створення користувача
     */

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<UserEntity> createUser(HttpEntity<UserEntity> entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)){
            UserDAO userDAO = daoHolder.getUserDAO();
            UserEntity userEntity = userDAO.add(entity.getBody());
            return new ResponseEntity<UserEntity>(userEntity, HttpStatus.OK);
        }

        return new ResponseEntity<UserEntity>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на видалення користувача
     */

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deleteUser(@PathVariable("id") long id, HttpEntity entity) {
        HttpHeaders headers = entity.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            UserDAO userDAO = daoHolder.getUserDAO();
            Boolean result;

            if (userDAO.get(id) == null) {
                result = false;
            }
            result = true;
            userDAO.delete(id);

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Метод обробки запиту на оновлення користувача
     */

    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> updateUser(@PathVariable long id, HttpEntity<UserEntity> user) {
        HttpHeaders headers = user.getHeaders();

        if(authorization.checkAccess(headers, true)) {
            UserDAO userDAO = daoHolder.getUserDAO();
            Boolean result;

            if (userDAO.get(id) == null) {
                result = false;
            }
            result = true;
            userDAO.update(user.getBody());

            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }
        return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);
    }
}
