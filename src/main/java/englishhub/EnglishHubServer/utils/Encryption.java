package englishhub.EnglishHubServer.utils;


import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Alexey on 4/18/2017.
 * Клас призначений для шифрування даних
 */
public class Encryption {

    //шифрує строкові дані за допомогаю алгоритму MD5
    public static String encryptMD5(final String data) {
        byte[] encryptedData = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("md5");
            encryptedData = md5.digest(data.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return toHexString(encryptedData);
    }

    //приводить зашифровані байти до строки
    private static String toHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();

        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }
}
