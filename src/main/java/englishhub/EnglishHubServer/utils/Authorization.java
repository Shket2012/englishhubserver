package englishhub.EnglishHubServer.utils;

import englishhub.EnglishHubServer.model.dao.impl.AccountDAO;
import englishhub.EnglishHubServer.model.entity.AccountEntity;
import org.springframework.http.HttpHeaders;

import javax.validation.constraints.NotNull;

/**
 * Клас призначений для авторизації користувача
 */

public class Authorization {

    //забезпечує отримання доступу до таблиці аккаунти
    private AccountDAO accountDAO;

    //ініціалізація
    public Authorization(@NotNull final AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    //перевіряє авторизаційні дані
    public boolean checkAccess(final AccountEntity authData, final boolean mustBeAdmin) {
        return checkAccess(authData.getEncryptedLogin(), authData.getEncryptedPassword(),
                mustBeAdmin);
    }

    //перевіряє авторизаційні дані
    public boolean checkAccess(final String encryptedLogin, final String encryptedPassword,
                               boolean checkAdmin) {

        AccountEntity account = accountDAO.getByLogin(encryptedLogin);

        if (account != null) {
            if (account.getEncryptedLogin().equals(encryptedLogin) &&
                    account.getEncryptedPassword().equals(encryptedPassword)) {
                if (checkAdmin) {
                    return account.isAdmin();
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    //перевіряє авторизаційні дані
    public boolean checkAccess(final HttpHeaders headersWithAuthData, boolean checkAdmin) {
        String login = headersWithAuthData.get("login").get(0);
        String password = headersWithAuthData.get("password").get(0);

        return checkAccess(login, password, checkAdmin);
    }
}
