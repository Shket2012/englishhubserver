package englishhub.EnglishHubServer.utils;

import ch.qos.logback.core.status.Status;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Клас приначений для утримання єдиного екземпляру EntityManagerFactory
 */

public class HibernateUtil {
    private static EntityManagerFactory factory;

    static {
        try {
            factory = Persistence.createEntityManagerFactory("persistUnitName");
        } catch (Exception exc) {
            Logger logger = Logger.getLogger("HibernateUtil Logger");

            if (exc instanceof org.hibernate.service.spi.ServiceException) {
                logger.log(Level.WARNING, exc.getMessage());
                System.exit(Status.ERROR);
            } else {
                throw new RuntimeException(exc.getMessage(), exc);
            }
        }
    }

    //забезпечує доступ до єкземпляру
    public static EntityManagerFactory getEntityManagerFactory() {
        return factory;
    }

    //закриває підключення
    public static void shutDown() {
        factory.close();
    }
}
