package englishhub.EnglishHubServer;

import englishhub.EnglishHubServer.model.dao.impl.UserDAO;
import englishhub.EnglishHubServer.model.entity.UserEntity;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Клас для тестування UserDAOTest
 */
public class UserDAOTest {
    private UserDAO service = new UserDAO();

    //Тестування збереження UserEntity
    @Test
    public void testSaveRecord() throws Exception {
        //Створюємо сутності
        UserEntity entity = new UserEntity();
        entity.setLastName("Sergeev");
        entity.setMiddleName("Vladimirovich");
        entity.setFirstName("Alexey");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        String dateInString = "11-02-1998";
        Date date = sdf.parse(dateInString);

        entity.setBirthDate(date);

        //Записали в БД
        UserEntity entityFromDB = service.add(entity);

        //Перевіряємо дані
        assertNotNull(entityFromDB);
    }

    //Тестування видалення UserEntity
    @Test
    public void testDeleteRecord() throws Exception {
        UserEntity entity = new UserEntity();
        entity.setLastName("Sergeev");
        entity.setMiddleName("Vladimirovich");
        entity.setFirstName("Alexey");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        String dateInString = "11-02-1998";
        Date date = sdf.parse(dateInString);

        entity.setBirthDate(date);

        //Додаємо
        UserEntity entityFromDB = service.add(entity);

        //Видаляємо
        service.delete(entityFromDB.getId());
    }

    //Тестування вибору UserEntity
    @Test
    public void testSelect() throws Exception {
        UserEntity entity = new UserEntity();
        entity.setLastName("Sergeev");
        entity.setMiddleName("Vladimirovich");
        entity.setFirstName("Alexey");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        String dateInString = "11-02-1998";
        Date date = sdf.parse(dateInString);

        entity.setBirthDate(date);

        //Додаємо
        UserEntity entityFromDB = service.add(entity);

        //Отримуємо
        UserEntity resultEntity = service.get(entityFromDB.getId());
        System.out.println(resultEntity);
    }

    //Тестування оновлення UserEntity
    @Test
    public void testUpdate() throws Exception {
        UserEntity entity = new UserEntity();
        entity.setLastName("Sergeev");
        entity.setMiddleName("Vladimirovich");
        entity.setFirstName("Alexey");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        String dateInString = "11-02-1998";
        Date date = sdf.parse(dateInString);

        entity.setBirthDate(date);

        //Додаємо запис
        UserEntity entityFromDB = service.add(entity);

        entityFromDB.setLastName("Petrov");

        //Оновлюємо запис
        service.update(entityFromDB);

        //Отримуємо запис
        UserEntity updatedEntity = service.get(entityFromDB.getId());

        //Перевіряємо результати
        assertNotNull(updatedEntity);
    }

    //Тестування отримання всіх сутностей UserEntity
    @Test
    public void testGetAll() throws Exception {
        UserEntity entity1 = new UserEntity();
        entity1.setLastName("Sergeev");
        entity1.setMiddleName("Vladimirovich");
        entity1.setFirstName("Alexey");

        SimpleDateFormat dateFormater = new SimpleDateFormat("dd-M-yyyy");
        String dateInString = "11-02-1998";
        Date date = dateFormater.parse(dateInString);

        entity1.setBirthDate(date);

        UserEntity entity2 = new UserEntity();
        entity2.setLastName("Nazarenko");
        entity2.setMiddleName("Edvard");
        entity2.setFirstName("Alexandrovich");

        String dateInString2 = "11-12-1997";
        Date date2 = dateFormater.parse(dateInString2);

        entity2.setBirthDate(date2);

        //Додаємо до БД
        service.add(entity1);
        service.add(entity2);

        //Отримуємо всі записи з БД
        List<UserEntity> userEntities = service.getAll();

        //Перевіряємо результати
        assertNotNull(userEntities);
        assertEquals(2, userEntities.size());
    }
}
