package englishhub.EnglishHubServer;


import englishhub.EnglishHubServer.model.dao.impl.MessageDAO;
import englishhub.EnglishHubServer.model.dao.impl.NewsDAO;
import englishhub.EnglishHubServer.model.entity.MessageEntity;
import englishhub.EnglishHubServer.model.entity.NewsEntity;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertNotNull;

/**
 * Клас для тестування MessageDAO
 */
public class MessageDAOTest {
    MessageDAO messageDAO = new MessageDAO();

    @Test
    public void testGetLastMessages() throws Exception {
        MessageDAO messageDAO = new MessageDAO();

        MessageEntity messageEntity = messageDAO.getLastByConversation(1L);
        assertNotNull(messageEntity);
    }
}
