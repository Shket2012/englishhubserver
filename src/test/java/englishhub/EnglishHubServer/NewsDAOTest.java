package englishhub.EnglishHubServer;


import englishhub.EnglishHubServer.model.dao.impl.NewsDAO;
import englishhub.EnglishHubServer.model.entity.NewsEntity;
import org.junit.Test;

import java.util.Date;

/**
 * Клас для тестування NewsDAO
 */
public class NewsDAOTest {
    NewsDAO service = new NewsDAO();

    //Тестування збереження NewsEntity у базі
    @Test
    public void testSaveRecord() throws Exception {
        //Создаем автомобиль для записи в БД
        NewsEntity entity = new NewsEntity();
        entity.setTitle("The way people tilt their smartphone 'can give away passwords and pins'");
        entity.setContent("The way you tilt your mobile while you're using it could allow hackers to work out your pin numbers and passwords");
        entity.setReleaseDate(new Date());

        //Додаємо в БД
        NewsEntity entityFromDB = service.add(entity);

        //Виводимо записанну до БД сутність
        System.out.println(entityFromDB);
    }

    //Тестування видалення NewsEntity з бази
    @Test
    public void testDeleteRecord() throws Exception {
        //Створюємо сутність для додавання
        NewsEntity entity = new NewsEntity();
        entity.setTitle("The way people tilt their smartphone 'can give away passwords and pins'");
        entity.setContent("The way you tilt your mobile while you're using it could allow hackers to work out your pin numbers and passwords");
        entity.setReleaseDate(new Date());
        //Додаємо до БД
        NewsEntity entityFromDB = service.add(entity);

        //Видаляємо з БД
        service.delete(entityFromDB.getId());
    }

    //Тестування отримання NewsEntity з бази
    @Test
    public void testSelect() throws Exception {
        //Створюємо сутність для додавання
        NewsEntity entity = new NewsEntity();
        entity.setTitle("The way people tilt their smartphone 'can give away passwords and pins'");
        entity.setContent("The way you tilt your mobile while you're using it could allow hackers to work out your pin numbers and passwords");
        entity.setReleaseDate(new Date());

        //Додаємо до БД
        NewsEntity entityFromDB = service.add(entity);

        //Отримуємо з БД
        NewsEntity resultEntity = service.get(entityFromDB.getId());
        System.out.println(resultEntity);
    }

    //Тестування оновлення NewsEntity у базі
    @Test
    public void testUpdate() throws Exception {
        //Створюємо сутність для додавання
        NewsEntity entity = new NewsEntity();
        entity.setTitle("The way people tilt their smartphone 'can give away passwords and pins'");
        entity.setContent("The way you tilt your mobile while you're using it could allow hackers to work out your pin numbers and passwords");
        entity.setReleaseDate(new Date());

        //Додаємо до БД
        NewsEntity entityFromDB = service.add(entity);

        //Змінюємо дані
        entityFromDB.setContent("Second Article");

        //Оновлюємо
        service.update(entityFromDB);

        //Отримуємо оновлену сутність
        NewsEntity updatedEntity = service.get(entityFromDB.getId());
        System.out.println(updatedEntity);
    }
}
