package englishhub.EnglishHubServer;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Alexey on 5/25/2017.
 */
public class UnitTest {
    String namePath = "25052017003928.png";

    @Test
    public void testFile() {
        byte[] data = null;
        Path fileLocation = Paths.get(namePath);
        try {
            data = Files.readAllBytes(fileLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertNotNull(data);
    }
}
