package englishhub.EnglishHubServer;

import englishhub.EnglishHubServer.model.dao.impl.AccountDAO;
import englishhub.EnglishHubServer.model.dao.impl.UserDAO;
import englishhub.EnglishHubServer.model.entity.AccountEntity;
import englishhub.EnglishHubServer.model.entity.UserEntity;
import englishhub.EnglishHubServer.utils.Encryption;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Класс для тестирования AccountDAO
 */
public class AccountDAOTest {
    private AccountDAO accountDAO = new AccountDAO();

    //Тестирование получения AccountEntity по логину
    @Test
    public void testGetByLogin() throws Exception {
        UserDAO userDAO = new UserDAO();
        AccountEntity account = new AccountEntity(userDAO.get(16L), Encryption.encryptMD5("login"),
                Encryption.encryptMD5("password"), true);

        //добавление аккаунта в базу данных
        accountDAO.add(account);
        AccountEntity foundAccount = accountDAO.getByLogin(Encryption.encryptMD5("login"));

        //проверка на существование добавленной записи в базе данных
        assertEquals(account.getEncryptedLogin(), foundAccount.getEncryptedLogin());
    }

    //Тестирование получения UserEntity из AccountEntity
    @Test
    public void testGetUserFromAccount() throws Exception {
        AccountDAO accountDAO = new AccountDAO();
        UserEntity userEntity = accountDAO.getByLogin(Encryption.encryptMD5("login")).getUser();
        assertNotNull(userEntity);
    }

    @Test
    public void testGetAllUsersByGroup() throws Exception {

        List<UserEntity> users = accountDAO.getAllUsersByGroup(102);

        assertNotNull(users);
        assertEquals(3, users.size());
    }

    @Test
    public void testUpdateAccount() throws Exception {

        UserEntity userFromBody = new UserEntity("Test", "Test", "Test", new Date());

        AccountEntity accountFromBody = new AccountEntity(userFromBody, "test","test");
        AccountEntity receivedAccount = accountDAO.get(218L);

        assertNotNull(receivedAccount);

        receivedAccount.setEncryptedLogin(accountFromBody.getEncryptedLogin());
        receivedAccount.setEncryptedPassword(accountFromBody.getEncryptedPassword());

        accountDAO.update(receivedAccount);

        assertEquals(accountFromBody.getEncryptedLogin(), receivedAccount.getEncryptedLogin());
    }
}
